//
//  JKViewController.m
//  JKPopup
//
//  Created by Joris Kluivers on 3/2/12.
//  Copyright (c) 2012 Cardcloud. All rights reserved.
//

#import "JKViewController.h"

#import "JKPopup.h"
#import "JKProgressHUD.h"

@interface JKViewController ()
@property(nonatomic, strong) JKPopup *popup;
@end

@implementation JKViewController

@synthesize popup;

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)showPopup:(id)sender {
	self.popup = [[JKProgressHUD alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
	[self.popup show];
	
	[self performSelector:@selector(hidePopup:) withObject:nil afterDelay:3.0];
}

- (void) hidePopup:(id)sender {
	[self.popup hide];
}

@end
